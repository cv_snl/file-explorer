const fs = require('fs');
const { join, parse } = require('path');
const EventEmitter = require('events');

class DirService extends EventEmitter{
  constructor(dir = null) {
    super();
    this.dir = dir || process.cwd();
  }

  static readDir(dir) {
    const fInfoArr = fs.readdirSync(dir, 'utf-8').map(filename => {
      const filePath = join(dir, filename);
      const stats = DirService.getStats(filePath);

      if (stats === false) {
        return false;
      }

      return {
        filename,
        stats
      }
    });

    return fInfoArr.filter(item => item !== undefined);
  }

  static getStats(filePath) {
    try {
      return fs.statSync(filePath);
    } catch (err) {
      return false;
    }
  }

  setDir(dir = ' ') {
    let newDir = path.join(this.dir, dir);

    if (DirService.getStats(newDir) === false) {
      return;
    }

    this.dir = newDir
    this.notify();
  }

  notify() {
    this.emit('update');
  }

  getDirlist() {
    const collection = DirService.readDir(this.dir).filter(fInfo => fInfo.stats.isDirectory());

    if (!this.isRoot()) {
      collection.unshift({ fileName: '..' });
    }

    return collection;
  }

  getFileList() {
    return DirService.readDir(this.dir).filter(fInfo => fInfo.stats.isFile());
  }

  isRoot() {
    const { root } = parse(this.dir);

    return (root === this.dir);
  }
}

exports.DirService = DirService;